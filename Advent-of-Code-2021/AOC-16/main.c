#include <stdio.h>
#include <stdlib.h>

#if 1
    #define FILE_PATH "example.txt"
#else
    #define FILE_PATH "input.txt"
#endif

void parse()
{
    FILE *fp = fopen(FILE_PATH, "r");
    if(!fp) {
        fprintf(stderr, "ERROR: Could not open file: %s\n", FILE_PATH);
        exit(EXIT_FAILURE);
    }

    fclose(fp);
}

int main(void)
{
    parse();
    return 0;
}
