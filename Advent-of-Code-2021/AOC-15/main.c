#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define min(a, b) (((a) < (b)) ? (a) : (b))

#if 0
  #define N 1
#else
  #define N 5
#endif


// part 2 doesnt work with input for some reason bruh
#if 1
  #define FILE_P "sample.txt"
  #define T_CAP 10 * N
  #define T_CAP_S 10
#else
  #define FILE_P "input.txt"
  #define T_CAP 100 * N
  #define T_CAP_S 100
#endif

int table[T_CAP][T_CAP];

void ParseInput(char *filepath)
{
  char ch;
  FILE *fp;
  fp = fopen(filepath, "r");

  if(fp == NULL)
  {
    fprintf(stderr, "ERROR: something with file idk fuck you");
    exit(EXIT_FAILURE);
  }

  int i = 0;
  int j = 0;
  while((ch = fgetc(fp)) != EOF)
  {
    if(ch == '\n') { i += 1; j = 0; continue; }

    table[i][j] = (ch - '0');
    j += 1;
  }
  
  fclose(fp);
}

void FindRisk()
{
  // dont add to total risk
  table[0][0] = 0;
  
  for(int i=1; i<T_CAP; i++) table[i][0] += table[i-1][0];
  for(int j=1; j<T_CAP; j++) table[0][j] += table[0][j-1];

  for(int i=1; i<T_CAP; i++)
  {
    for(int j=1; j<T_CAP; j++)
    {
      table[i][j] += min(table[i][j-1], table[i-1][j]);
    }
  }

}

void Part1()
{
  ParseInput(FILE_P);
  FindRisk();
  printf("PART1: %d\n", table[T_CAP-1][T_CAP-1]); 
}

void ReconstructMap()
{
  for(int j=0; j<T_CAP_S; j++)
    for(int n=T_CAP_S; n<T_CAP; n+=T_CAP_S)
      for(int i=0; i<T_CAP_S; i++)
      {
	if(table[i+n-T_CAP_S][j] != 9)
	  table[i+n][j] = table[i+n-T_CAP_S][j] + 1;
	else
	  table[i+n][j] = 1;
      }

  for(int i=0; i<T_CAP; i++)
    for(int n=T_CAP_S; n<T_CAP; n+=T_CAP_S)
      for(int j=0; j<T_CAP_S; j++)
      {
	if(table[i][j+n-T_CAP_S] != 9)
	  table[i][j+n] = table[i][j+n-T_CAP_S] + 1;
	else
	  table[i][j+n] = 1;
      }
}

void Part2()
{
  ParseInput(FILE_P);
  ReconstructMap();
  FindRisk();
  printf("PART2: %d\n", table[T_CAP-1][T_CAP-1]);
}

void PrintTable()
{
  for(int i=0; i<T_CAP; i++)
  {
    for(int j=0; j<T_CAP; j++)
    {
      printf("%d", table[i][j]);
    }
    printf("\n");
  }
}

int main(void)
{
  memset(table, 0, sizeof(table));
  //  Part1();
  Part2();
 
  return 0;
}

