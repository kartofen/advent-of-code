#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int pos = 0;
int depth = 0;
int aim = 0;

void ParseInput(char *filepath)
{
  char ch;
  FILE *fp;
  fp = fopen(filepath, "r");

  if (fp == NULL)
  {
    perror("Error while opening the file.\n");
    exit(EXIT_FAILURE);
  }

  char command;
  char char_x_val;
  int current_type = 0; // 0 - command; 1 - skip; 2 - val
  /* Part 1
  while((ch= fgetc(fp)) != EOF)
  {
    if(ch == ' ')
    {
      current_type = 2;
    }
    else if(ch == '\n')
    {
      int x = atoi(&char_x_val);
      switch(command)
      {
      case 'f':
	pos += x;
	break;
      case 'd':
	depth += x;
	break;
      case 'u':
	depth -= x;
      }

      current_type = 0;
    }
    else if(current_type == 0)
    {
      command = ch;
      current_type = 1;
    }
    else if(current_type == 2)
    {
      char_x_val = ch;
    }
  }
  */

  /* Part2 */
   while((ch= fgetc(fp)) != EOF)
  {
    if(ch == ' ')
    {
      current_type = 2;
    }
    else if(ch == '\n')
    {
      int x = atoi(&char_x_val);
      switch(command)
      {
      case 'f':
	pos += x;
	depth += aim * x;
	break;
      case 'd':
	aim += x;
	break;
      case 'u':
	aim -= x;
      }

      current_type = 0;
    }
    else if(current_type == 0)
    {
      command = ch;
      current_type = 1;
    }
    else if(current_type == 2)
    {
      char_x_val = ch;
    }
  }
  
  fclose(fp);
}

int main(void)
{
  ParseInput("input-2.txt");
  printf("pos: %d, depth: %d; product: %d", pos, depth, pos * depth);
  return 0;
}
