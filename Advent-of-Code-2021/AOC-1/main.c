#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 5000

int numbers[N];

void ParseInput(char *filepath)
{
  char ch;
  FILE *fp;
  fp = fopen(filepath, "r");

  if (fp == NULL)
  {
    perror("Error while opening the file.\n");
    exit(EXIT_FAILURE);
  }

  int i = 0;
  int n = 0;
  char *str = malloc(sizeof(char)*N);
  while((ch= fgetc(fp)) != EOF)
  {
    if(ch != '\n')
    {
      str[i] = ch;
      i += 1;
    }
    else
    {
      i = 0;
      numbers[n] = atoi(str);
      n += 1;
      memset(str, ' ', sizeof(char)*N);
    }
  }
  
  free(str);
  
  fclose(fp);
}

void PrintNumbers()
{
  for(int i=0; i<N; i++)
  {
    printf("%d\n", numbers[i]);
  }
}

int main (void)
{
  ParseInput("input-1.txt");

  /* Part One
  int bigger = 0;
  for(int i=1; i<N; i++)
  {
    if(numbers[i] > numbers[(i-1)])
      bigger += 1;
  }

  printf("Answer: %d", bigger);
 */

  // Part Two
  int bigger = 0;
  for(int i = 3; i<N; i++)
  {
    if(
       (numbers[i-2] + numbers[i-1] + numbers[i]) >
       (numbers[i-3] + numbers[i-2] + numbers[i -1]))
      bigger += 1;
  }

  printf("Answer: %d", bigger);
  
  return 0;
}
