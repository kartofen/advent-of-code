#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdint.h>

#define N 5
#define INP_COUNT 12

int table[N];

void ParseInput(char *filepath)
{
  char ch;
  FILE *fp;
  fp = fopen(filepath, "r");

  if(fp == NULL)
  {
    fprintf(stderr, "ERROR: something with file idk what fuck you");
    exit(EXIT_FAILURE);
  }

  int i = 0;
  while((ch = fgetc(fp)) != EOF)
  {
    if(ch == '\n') { i = 0; continue; }

    if(ch == '1')
      table[i] += 1;

    i += 1;
  }

  fclose(fp);
}

void PrintTable()
{
  for(int i=0; i<N; i++)
  {
    printf("bitplace %d: %d\n", i+1, table[i]);
  }
}

int PartOne()
{
  int gamma_val = 0;
  for(int i=0; i<N; i++)
  {
    assert(table[i] != (INP_COUNT/2));
      
    if(table[i] > (INP_COUNT/2))
    {
      gamma_val = gamma_val | (1 << (N-1-i));
    }
  }

  int epsilon_val = gamma_val;
  for(int i=0; i<N; i++)
  {
    epsilon_val = epsilon_val ^ (1 << i);
  }

  int result = gamma_val * epsilon_val;  
  return result;  
}

int PartTwo()
{
  
  
  int result = 0;
  return result;
}

int main(void)
{
  memset(table, 0, sizeof(int)*N);
  ParseInput("sample.txt");

  PrintTable();
  printf("Part 1-RESULT: %d\n", PartOne());
 
  return 0;
}
