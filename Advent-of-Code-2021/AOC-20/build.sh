#!/bin/sh

set -xe

gcc -o main -Wall -Wextra main.c -g

./main
