#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#if 0
#define PFILE "sample.txt"
#else
#define PFILE "input.txt"
#endif

#define T_CAP 100
char template[T_CAP];
size_t tmpl_sz = 0;

#define R_CAP 100
typedef struct {
  char inp[2];
  char ch;
} rule_t;
rule_t rules[R_CAP];
size_t rules_sz = 0;

#define CAP 1000000
typedef uint64_t lsize_t;
char product[CAP];
lsize_t prod_sz = 0;


void Parse()
{

  // some init
  memset(template, '\0', T_CAP);
  memset(product,  '\0', CAP);

  char ch;
  FILE *fp;
  fp = fopen(PFILE, "r");

  int mode = 0;  // 0 - for template; 1 - for rules; 2 - intermid
  int mode_s = 0; // 0 - has not encountered ' ';  1 - after ' '
  while((ch = fgetc(fp)) != EOF)
  {
    if(ch == '\n' && mode == 0)
    {
      mode = 2;
    }
    else if(ch == '\n' && mode == 2)
    {
      mode = 1;
    }
    else if(ch == '\n' && mode == 1)
    {
      memset(rules[rules_sz].inp, '\0', 2);
      rules[rules_sz].ch = '\0';
    }
    else if(ch != '\n' && mode == 0)
    {
      template[tmpl_sz] = ch;
      tmpl_sz += 1;
    }
    else if(ch != '\n' && mode == 1)
    {
      switch(mode_s)
      {
      case 0:
        if(rules[rules_sz].inp[0] == '\0')
        {
          rules[rules_sz].inp[0] = ch;
        }
        else
        {
          rules[rules_sz].inp[1] = ch;
          mode_s = 1;
        }
        break;
      case 1:
        if(ch != ' ' && ch != '-' && ch != '>')
        {
          rules[rules_sz].ch = ch;
          rules_sz += 1;
          mode_s = 0;
        }
      }
    }
  }

  fclose(fp);
}

void InsertAt(char ch, size_t pos)
{
  for(lsize_t i = prod_sz; i > pos; i--)
  {
    product[i] = product[i-1];
  }
  product[pos] = ch;

  prod_sz += 1;
}

void Part1()
{
  Parse();
  prod_sz = tmpl_sz;
  memcpy(product, template, prod_sz);

  char elements[CAP];  memset(elements, '\0', CAP);
  lsize_t positions[CAP];
  lsize_t inrt_sz = 0;

  printf("%s\n", product);

  for(int step = 0; step < 40; step++)
  {
    for(lsize_t i = 0; i < (prod_sz-1); i++)
    {
      for(int j = 0; j < rules_sz; j++)
      {
        if(product[i]   == rules[j].inp[0] &&
           product[i+1] == rules[j].inp[1])
        {
          elements[inrt_sz]  = rules[j].ch;
          positions[inrt_sz] = i + 1 + inrt_sz; //error potential
          inrt_sz += 1;
          break;
        }
      }
    }
    for(lsize_t i = 0; i < inrt_sz; i++)
    {
      InsertAt(elements[i], positions[i]);
    }
    inrt_sz = 0;
    memset(elements, '\0', CAP);
  }

  // Get count and subtract
  lsize_t el_occurence[10];
  /*   S - 0 */
  /*   H - 1 */
  /*   N - 2 */
  /*   C - 3 */
  /*   O - 4 */
  /*   P - 5 */
  /*   F - 6 */
  /*   B - 7 */
  /*   V - 8 */
  /*   K - 9 */

  for(lsize_t i = 0; i < prod_sz; i++)
  {
    switch(product[i])
    {
    case 'S': el_occurence[0] += 1; break;
    case 'H': el_occurence[1] += 1; break;
    case 'N': el_occurence[2] += 1; break;
    case 'C': el_occurence[3] += 1; break;
    case 'O': el_occurence[4] += 1; break;
    case 'P': el_occurence[5] += 1; break;
    case 'F': el_occurence[6] += 1; break;
    case 'B': el_occurence[7] += 1; break;
    case 'V': el_occurence[8] += 1; break;
    case 'K': el_occurence[9] += 1; break;
    }
  }

  //get biggest
  lsize_t biggest = 0;
  for(int i = 0; i < 10; i++)
  {
    int counter = 0;
    for(int j = 0; j < 10; j++)
      if(el_occurence[i] >= el_occurence[j]) counter += 1;
    if(counter == 10) { biggest = el_occurence[i]; break; }
  }

  //get smallest
  lsize_t smallest = 0;
  for(int i = 0; i < 10; i++)
  {
    int counter = 0;
    for(int j = 0; j < 10; j++)
      if(el_occurence[i] <= el_occurence[j]) counter += 1;
    if(counter == 10) {smallest = el_occurence[i]; break; }
  }

  lsize_t result = biggest - smallest;
  printf("%llu-%llu=%llu", biggest, smallest, result);
}

void Part2()
{

}

int main(void)
{
  Part1();
  return 0;
}
