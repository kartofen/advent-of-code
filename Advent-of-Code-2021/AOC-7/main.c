#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CAP 3000

#if 0
#define PFILE "sample.txt"
#else
#define PFILE "input.txt"
#endif

int vals[CAP];
size_t vals_sz = 0;

void Parse()
{
  // Parse
  char ch;
  FILE *fp;
  fp = fopen(PFILE, "r");

  char int_buf[6];
  memset(int_buf, '\0', sizeof(int_buf));
  int n = 0;
  while((ch = fgetc(fp)) != EOF)
  {
    if(ch != ',')
    {
      int_buf[n] = ch;
      n += 1;
      continue;
    }

    vals[vals_sz] = atoi(int_buf);
    vals_sz += 1;
    memset(int_buf, '\0', sizeof(int_buf));
    n = 0;
  }

}

void Part1()
{
  Parse();
  size_t fuel_arr[CAP];
  size_t fuel = 0;

  for(int i = 0; i < CAP; i++)
  {
    int target = i;
    for(int j = 0; j < vals_sz; j++)
      fuel += abs(target - vals[j]);

    fuel_arr[i] = fuel;
    fuel = 0;
  }

  // Find the smallest in the fuel array
  for(size_t i = 0; i < CAP; i++)
  {
    size_t counter = 0;
    for(int j = 0; j < CAP; j++)
    {
      if(fuel_arr[i] <= fuel_arr[j])
        counter += 1;
    }

    if(counter == CAP)
    {
      printf("%d\n", fuel_arr[i]);
      break;
    }
  }
}

size_t CountFuel(int target)
{
  // somehow works idk
  size_t sum = 0;
  for(size_t i = 1; i <= target; i++)
    sum += i;
  return sum;
}

void Part2()
{
  Parse();
  size_t fuel_arr[CAP];
  size_t fuel = 0;

  for(int i = 0; i < CAP; i++)
    {
      size_t target = i;
      for(int j = 0; j < vals_sz; j++)
        fuel += CountFuel(abs(target - vals[j]));

      fuel_arr[i] = fuel;
      fuel = 0;
    }

  // Find the smallest in the fuel array
  for(size_t i = 0; i < CAP; i++)
    {
      size_t counter = 0;
      for(size_t j = 0; j < CAP; j++)
        {
          if(fuel_arr[i] <= fuel_arr[j])
            counter += 1;
        }

      if(counter == CAP)
        {
          printf("%d: %d\n", i, fuel_arr[i]);
          break;
        }
    }
}

int main()
{
  Part2();
  return 0;
}
