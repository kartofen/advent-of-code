#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <stdint.h>

#define XLEN 139 //10
#define YLEN 137 //9
#define LEN (XLEN * YLEN)

typedef struct Sea_cuc_move {
  int from_pos;
  int to_pos;
} Sea_cuc_move;

char map[LEN];
char map_bak[LEN];

void ParseInput(char *filepath)
{
  char ch;
  FILE *fp;
  fp = fopen(filepath, "r");

  if(fp == NULL)
  {
    fprintf(stderr, "ERROR: something with file idk what fuck you");
    exit(EXIT_FAILURE);
  }

  int i = 0;
  while((ch = fgetc(fp)) != EOF)
  {
    if(ch == '\n') continue;

    map[i] = ch;
    i++;
  }

  fclose(fp);
}

void Move(char type, Sea_cuc_move pos)
{
  map[pos.from_pos] = '.';
  map[pos.to_pos] = type;
}

void MoveEast()
{
  Sea_cuc_move positions_east[5000];
  int cucs_east = 0;
  
  for(int i=0; i<LEN; i++)
  {
    if(map[i] == '>')
    {
      int x = i % XLEN;
      if(x == (XLEN-1))
      {
	if(map[i - x] != '.')
	  continue;
	
	positions_east[cucs_east] = (Sea_cuc_move){i, i - x};
	cucs_east += 1;
      }
      else if(map[i+1] == '.')
      {
	positions_east[cucs_east] = (Sea_cuc_move){i, i + 1};
	cucs_east += 1;
      }
	
    }
  }

  assert(cucs_east  < 5000);

  if(cucs_east != 0)
    for(int i=0; i<cucs_east; i++)
      Move('>', positions_east[i]);
}

void MoveSouth()
{
  Sea_cuc_move positions_south[5000];
  int cucs_south = 0;
  
  for(int i=0; i<LEN; i++)
  {
    if(map[i] == 'v')
    {
      int y = floor(i / XLEN);
      if(y == (YLEN-1))
      {
	if(map[i%XLEN] != '.')
	   continue;

        positions_south[cucs_south] = (Sea_cuc_move){i, i%XLEN};
        cucs_south += 1;	
      }
      else if (map[i+XLEN] == '.')
      {
	positions_south[cucs_south] = (Sea_cuc_move){i, i+XLEN};
        cucs_south +=1;
      }
    }
  }

  assert(cucs_south < 5000);
  
  if(cucs_south != 0)
    for(int i=0; i<cucs_south; i++)
      Move('v', positions_south[i]);
}

void NextStep()
{
  MoveEast();
  MoveSouth();
}

void PrintMap()
{
  for(int i=0; i<LEN; i++)
  {
    if((i % XLEN) == 0)
      printf("\n");
    printf("%c", map[i]);
  }
  printf("\n\n");
}
    
		
int main()
{  
  ParseInput("input.txt");

  for(u_int64_t i=0; i < 10000; i++)
  {
    memcpy(map_bak, map, sizeof(char) * LEN);
    NextStep();
    
    if(strcmp(map, map_bak) == 0)
    {
      printf("EQUAL after: %llu\n", i + 1);
      exit(0);
    }
  }

  PrintMap();
  
  return 0;
}
