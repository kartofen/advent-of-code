#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if 1
    #define PART_1
#else
    #define PART_2
#endif

#if 0
    #define FILE_PATH "example.txt"
    #define NUMS 12
    #define BITS 5
#else
    #define FILE_PATH "input.txt"
    #define NUMS 1000
    #define BITS 12
#endif

int nums[NUMS] = {0};

void parse()
{
    FILE *fp = fopen(FILE_PATH, "r");
    if(!fp) {
        fprintf(stderr, "ERROR: Could not open file %s\n", FILE_PATH);
        exit(EXIT_FAILURE);
    }

    char line[32];
    int index = 0;
    while(fgets(line, sizeof(line), fp) != NULL)
    {
        for(int i = 0; i < BITS; i++)
            nums[index] |= (line[i] - '0') << (BITS-1 - i);

        index++;
    }

    fclose(fp);
}

ulong gen_gamma()
{
    int most_common[BITS] = {0};

    for(int i = 0; i < BITS; i++)
    {
        int ones = 0;

        for(int j = 0; j < NUMS; j++)
            if(nums[j] & (1 << i)) ones++;

        if(ones > NUMS/2) most_common[i] = 1;
    }

    ulong gamma = 0;

    for(int i = 0; i < BITS; i++)
        gamma |= most_common[i] << i;

    return gamma;
}

void print_nums()
{
    for(int i = 0; i < NUMS; i++)
        printf("%b\n", nums[i]);
}

void part_1()
{
    ulong gamma = gen_gamma();
    ulong epsilon = (~gamma & ~(~0 << BITS));

    printf("Power consumption is %lu", gamma*epsilon);
}

void part_2()
{
    exit(EXIT_FAILURE);
}

int main(void)
{
    parse();

#ifdef PART_1
    part_1();
#endif
#ifdef PART_2
    part_2();
#endif

    return 0;
}
