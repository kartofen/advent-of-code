#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>

#define N 9
#define MAXDAYS 256

u_int64_t table[N];
u_int64_t temp_table[N];

void PrintTable()
{
  for(int i=0; i<N; i++)
  {
    printf("%llu: %llu\n", i, table[i]);
  }
}

void NextDay()
{
  memset(temp_table, 0, sizeof(u_int64_t)*N);

  for(int i=1; i<N; i++)
  {
    temp_table[i-1] = table[i];
  }

  temp_table[6] += table[0];
  temp_table[8] += table[0];

  memcpy(table, temp_table, sizeof(u_int64_t)*N);
}

u_int64_t TableSize()
{
  u_int64_t size = 0;
  for(int i=0; i<N; i++)
  {
    size += table[i];
  }

  return size;
}

void ParseInput(char *input)
{
  for(int i=0; i<strlen(input); i++)
  {
    if(input[i] != ',')
      table[atoi(&(input[i]))] += 1;
  }
}

int main(void)
{
  memset(table, 0, sizeof(u_int64_t)*N);

  ParseInput("2,1,2,1,5,1,5,1,2,2,1,1,5,1,4,4,4,3,1,2,2,3,4,1,1,5,1,1,4,2,5,5,5,1,1,4,5,4,1,1,4,2,1,4,1,2,2,5,1,1,5,1,1,3,4,4,1,2,3,1,5,5,4,1,4,1,2,1,5,1,1,1,3,4,1,1,5,1,5,1,1,5,1,1,4,3,2,4,1,4,1,5,3,3,1,5,1,3,1,1,4,1,4,5,2,3,1,1,1,1,3,1,2,1,5,1,1,5,1,1,1,1,4,1,4,3,1,5,1,1,5,4,4,2,1,4,5,1,1,3,3,1,1,4,2,5,5,2,4,1,4,5,4,5,3,1,4,1,5,2,4,5,3,1,3,2,4,5,4,4,1,5,1,5,1,2,2,1,4,1,1,4,2,2,2,4,1,1,5,3,1,1,5,4,4,1,5,1,3,1,3,2,2,1,1,4,1,4,1,2,2,1,1,3,5,1,2,1,3,1,4,5,1,3,4,1,1,1,1,4,3,3,4,5,1,1,1,1,1,2,4,5,3,4,2,1,1,1,3,3,1,4,1,1,4,2,1,5,1,1,2,3,4,2,5,1,1,1,5,1,1,4,1,2,4,1,1,2,4,3,4,2,3,1,1,2,1,5,4,2,3,5,1,2,3,1,2,2,1,4");
  //  ParseInput("3,4,3,1,2");

  PrintTable();
  printf("Size: %llu\n", TableSize());
  printf("----------------------------\n");
 
  for(int i=0; i < MAXDAYS; i++) 
  {
    NextDay();
  }

  PrintTable();
  printf("Size: %llu\n", TableSize());
  printf("Day:  %llu\n", MAXDAYS);
  
  return 0;

}
