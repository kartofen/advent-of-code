#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#if 0
  #define PFILE "sample.txt"
  #define A_CAP 6
#else
  #define PFILE "input.txt"
  #define A_CAP 200
#endif

int ints[A_CAP];

void PrintInts()
{
  for(int i=0; i<A_CAP; i++)
    printf("%d\n", ints[i]);
}

void Parse()
{
  // Parse
  char ch;
  FILE *fp;
  fp = fopen(PFILE, "r");

  int i = 0;
  int n = 0;
  char *int_str = malloc(sizeof(char)*5);
  while((ch = fgetc(fp)) != EOF)
  {
    if(ch == '\n')
    {
      ints[i] = atoi(int_str);
      i += 1;
      n = 0;
      memset(int_str, 0, sizeof(char)*5);
      continue;
    }

    int_str[n] = ch;
    n += 1;
  }

  assert(i == A_CAP);

  free(int_str);
  fclose(fp);
}

void Part1()
{
  Parse();
  
  // Check
  for(int i=1; i<A_CAP; i++)
    for(int j=i; j<A_CAP; j++)
      if((ints[i-1] + ints[j]) == 2020)
	printf("RESULT-1: %llu\n", ints[i-1] * ints[j]);
}

void Part2()
{
  Parse();

  // Check
  for(int i=2; i<A_CAP; i++)
    for(int j=i; j<A_CAP; j++)
      for(int k=j; k<A_CAP; k++)
	if((ints[i-2] + ints[j-1] + ints[k]) == 2020)
	  printf("RESULT-2: %llu\n", ints[i-2] * ints[j-1] * ints[k]);
}

int main(void)
{
  Part1();
  Part2();
  return 0;
}
