#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if 0
#define PART_1
#else
#define PART_2
#endif

#if 0
#define FILE_PATH "example.txt"
#else
#define FILE_PATH "input.txt"
#endif

int q_answ = 0;

void parse()
{
    FILE *fp = fopen(FILE_PATH, "r");
    if(!fp) {
        fprintf(stderr, "ERROR: Could not open file: %s", FILE_PATH);
        exit(EXIT_FAILURE);
    }

    char questions[26] = "abcdefghijklmnopqrstuvwxyz";
    int answered[26] = {0};

#ifdef PART_2
    int ppl_in_group = 0;
#endif

    char line[32];
    while(fgets(line, sizeof(line), fp) != NULL)
    {
        if(line[0] == '\n' || strncmp(line, "EOF", 3) == 0) {
            for(int i = 0; i < 26; i++) {
            #ifdef PART_1
                q_answ += answered[i];
            #endif

            #ifdef PART_2
                if(answered[i] == ppl_in_group)
                     q_answ++;
            #endif
            }

        #ifdef PART_2
            ppl_in_group = 0;
        #endif
            memset(answered, 0, sizeof(answered));
            continue;
        }

        #ifdef PART_2
            ppl_in_group++;
        #endif

        for(int i = 0; i < strlen(line); i++)
            for(int j = 0; j < 26; j++)
                if(line[i] == questions[j]) {
                #ifdef PART_1
                    answered[j] = 1;
                #endif
                #ifdef PART_2
                    answered[j]++;
                #endif
                }
    }

    fclose(fp);
}

int main(void)
{
    parse();
    printf("%d questions answered\n", q_answ);
    return 0;
}
